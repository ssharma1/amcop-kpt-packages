# crossplane-aws

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] crossplane-aws`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree crossplane-aws`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init crossplane-aws
kpt live apply crossplane-aws --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
